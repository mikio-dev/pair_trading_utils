# Data Mangement for Encoder Decoder

# Extracting only a subset of features
def select_columns(df,cur_names):
    cols_to_keep = cur_names
    df_subset = df[cols_to_keep]
    return df_subset


# Some of the integer features need to be onehot encoded;
# but not all of them
def onehot_encode_integers(df, excluded_cols):
    import pandas as pd
    
    df = df.copy()

    int_cols = [col for col in df.select_dtypes(include=['int'])
                if col not in excluded_cols]

    df.loc[:, int_cols] = df.loc[:, int_cols].astype('str')

    df_encoded = pd.get_dummies(df)
    return df_encoded

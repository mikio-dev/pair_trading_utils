class encoderDecoder:

  def __init__(self,
               asset_names,
               window_len = 300, 
               forecast_len = 100, 
               encoder_units = 70,
               decoder_units = 70,
               dense_units = 70,
               n_dense_layers = 5,
               patience = 10,
               encoder_regularizer = None,
               decoder_regularizer = None,
               dense_regularizer = None):#, batch_size = 10):
    self.window_len = window_len
    self.forecast_len = forecast_len
    self.asset_names = asset_names
    self.asset_nums = len(self.asset_names)
    self.encoder_units = encoder_units
    self.decoder_units = decoder_units #self.asset_nums*10
    self.dense_units = dense_units #self.asset_nums*10
    self.encoder_regularizer = encoder_regularizer
    self.decoder_regularizer = decoder_regularizer
    self.dense_regularizer = dense_regularizer
    self.n_dense_layers = n_dense_layers #5
    self.patience = patience

  def create_dataset(self,df, n_deterministic_features,
                   #window_size, 
#                   forecast_size,
                   batch_size):
    import tensorflow as tf
    tf.random.set_seed(1)
    # Feel free to play with shuffle buffer size
    shuffle_buffer_size = len(df)
    # Total size of window is given by the number of steps to be considered
    # before prediction time + steps that we want to forecast
    total_size = self.window_len + self.forecast_len

    data = tf.data.Dataset.from_tensor_slices(df.values)

    # Selecting windows
    data = data.window(total_size, shift=1, drop_remainder=True)
    data = data.flat_map(lambda k: k.batch(total_size))

    # Shuffling data (seed=Answer to the Ultimate Question of Life, the Universe, and Everything)
    data = data.shuffle(shuffle_buffer_size, seed=42)

    # Extracting past features + deterministic future + labels
#    data = data.map(lambda k: ((k[:-forecast_size],
#                                k[-forecast_size:, -n_deterministic_features:]),
#                                k[-forecast_size:, :(len(df.columns) - n_deterministic_features)]))
#                               k[-forecast_size:, :3]))

    data = data.map(lambda k: ((k[:-self.forecast_len],
                                k[-self.forecast_len:, -n_deterministic_features:]),
                                k[-self.forecast_len:, :(len(df.columns) - n_deterministic_features)]))
#                               k[-self.forecast_len:, :3]))

    return data.batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)

  def compile(self,
              loss = None,
              optimizer = None):
    self.loss = loss
    self.optimizer = optimizer


  def fit(self,
          train_data, # train_dataははpandasののDataFrameを仮定
          val_data = None,
          epochs = None,
          batch_size = 10
          ):
    import tensorflow as tf
    from tensorflow.keras.callbacks import EarlyStopping
    import numpy as np
    self.batch_size = batch_size
    
    # Times at which to split train/validation and validation/test
  #    test_time = len(dataset) - window_len - forecast_len
  #    val_time = test_time - valid_period #  検証期間は１００日に固定する。とりあえず。
  #     val_time = test_time - window_len - forecast_len - valid_period

    # Auxiliary constants
    n_total_features = len(train_data.columns)
    n_aleatoric_features = self.asset_nums
    n_deterministic_features = n_total_features - n_aleatoric_features

    # Splitting dataset into train/val/test
    #training_data = dataset.iloc[:val_time]
    #validation_data = dataset.iloc[val_time:test_time]
    #test_data = dataset.iloc[test_time:]

    # Now we get training, validation, and test as tf.data.Dataset objects


    training_windowed = self.create_dataset(train_data,
                                       n_deterministic_features,
#                                       self.window_len,
                                       #self.forecast_len,
                                       self.batch_size)

    if type(val_data) != type(None):   
      validation_windowed = self.create_dataset(val_data,
                                     n_deterministic_features,
#                                     self.window_len,
                                     #self.forecast_len,
                                     self.batch_size)
  

    tf.random.set_seed(1)

    # First branch of the net is an lstm which finds an embedding for the past
    past_inputs = tf.keras.Input(shape=(self.window_len, n_total_features), name='past_inputs')
    # Encoding the past
    encoder = tf.keras.layers.LSTM(self.encoder_units, return_state=True, kernel_regularizer= self.encoder_regularizer)
    encoder_outputs, state_h, state_c = encoder(past_inputs)

    future_inputs = tf.keras.Input(shape=(self.forecast_len, n_deterministic_features), name='future_inputs')
    # Combining future inputs with recurrent branch output
    decoder_lstm = tf.keras.layers.LSTM(self.decoder_units, return_sequences=True, kernel_regularizer= self.decoder_regularizer)
    x = decoder_lstm(future_inputs,initial_state=[state_h, state_c])

    for i in np.arange(self.n_dense_layers):
        x = tf.keras.layers.Dense(self.dense_units, activation='tanh', kernel_initializer = 'glorot_normal', kernel_regularizer = self.dense_regularizer)(x)
    output = tf.keras.layers.Dense(self.asset_nums, activation='linear')(x)

    model = tf.keras.models.Model(inputs=[past_inputs, future_inputs], outputs=output)

    model.compile(loss=self.loss, optimizer=self.optimizer, metrics=["mse"])

    tf.random.set_seed(1)
    es = EarlyStopping(monitor = 'loss',
                     patience = self.patience,
                     verbose = 0)
    #history = 
    if type(val_data) != type(None):
        self.history = model.fit(training_windowed, 
                             epochs= epochs, #5, #10000, #25,
                             callbacks = [es],
                             validation_data = validation_windowed)
    else:
        self.history = model.fit(training_windowed, 
                             epochs= epochs, #5, #10000, #25,
                             callbacks = [es])

    self.model = model
  
  def evaluate(self,data):
    n_total_features = len(data.columns)
    n_aleatoric_features = self.asset_nums
    n_deterministic_features = n_total_features - n_aleatoric_features

    data_windowed = self.create_dataset(data,
                                   n_deterministic_features,
#                                   self.window_len,
                               #    self.forecast_len,
                                   batch_size = self.batch_size)
    evaluated_value = self.model.evaluate(data_windowed)

    return(evaluated_value)

  def predict(self,test_data):

    # Auxiliary constants
    n_total_features = len(test_data.columns)
    n_aleatoric_features = self.asset_nums
    n_deterministic_features = n_total_features - n_aleatoric_features

    test_windowed = self.create_dataset(test_data,
                                   n_deterministic_features,
#                                   self.window_len,
                                  # self.forecast_len,
                                   batch_size = self.batch_size)
    pred_test = self.model.predict(test_windowed)

    return(pred_test)



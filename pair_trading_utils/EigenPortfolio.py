def degeigval(eigen_vals,percent):
    sum_ev = sum(eigen_vals)
    exp_power = eigen_vals[0]/sum_ev
    degree = 1
    for i in range(1,len(eigen_vals)):
        exp_power = sum(eigen_vals[:degree])/sum_ev
        if exp_power >= percent:
            break
        else:
            degree += 1
    return(degree) 
    

# 主成分の影響を除去する制約下で期待リターンを最大化するポートフォリオを計算
# 目的関数は線形、すなわちE[R], ここでRはポートフォリオのリターン
def optportlin(mu, cov ,stock_num, exp_power = 0.95):
    import numpy as np
    import numpy.linalg as la
    from scipy.optimize import linprog
    # calculate eigen value 'l'(lambda) and eigen vector 'P' of matrix A
    l, P = la.eig(cov)
    degree = degeigval(l,exp_power)
  
    obj = -mu # 最小化問題を最大化問題に変換

    # constraint 
    rest_num = degree # number of restriction
    lhs_eq = np.append(P[:rest_num],[np.ones(stock_num)],axis = 0)
    rhs_eq = np.append(np.zeros(rest_num),1)

    bnd = [[-float('inf'),float('inf')]]*stock_num
    opt = linprog(c = obj, A_eq = lhs_eq, b_eq = rhs_eq, bounds = bnd, method = 'revised simplex')
    opt_port = opt.x

    return(opt_port)



# 目的関数を非線形な効用関数にした場合も実装する予定
def optportpower(self,mu, cov, stock_num, exp_power = 0.95, risk_aversion = 1):
    return(mu)
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, SimpleRNN,LSTM,GRU
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import EarlyStopping
import numpy as np

class RNN:
    def __init__(self,
                 forecast_len = 100,
                 window_len = 70,
                 type_RNN = LSTM,
                 asset_num = 7,
                 n_layers = 1,
                 units = 10,
                 patience = 10,
                 h_activation = 'tanh'):
        self.forecast_len = forecast_len
        self.window_len = window_len
        self.type_RNN = type_RNN
        self.optimizer = optimizers
        self.asset_num = asset_num
        self.n_layers = n_layers
        self.units = units
        self.patience = patience
        self.h_activation = h_activation

    def create_dataset(self,df_rate):
        y = df_rate.iloc[self.window_len:,:].values
        X = df_rate.iloc[:self.window_len,:].values.reshape(-1,self.window_len,self.asset_num)
        for i in range(1,len(df_rate) - self.window_len):
            X = np.append(X,df_rate.iloc[i:i+self.window_len,:].values.reshape(-1,self.window_len,self.asset_num),axis = 0)
        return(X,y)

    def compile(self,
                loss = 'mean_squared_error',
                optimizer = optimizers.Adam(learning_rate = 0.01,
                                              beta_1 = 0.9,
                                              beta_2 = 0.999,
                                              amsgrad = True)):
        self.loss = loss
        self.optimizer = optimizer

    def fit(self,
            X_train,y_train,
            X_val = None,
            y_val = None):
        self.model = Sequential()
        if self.n_layers > 1:
            for n_layer in range(1,self.n_layers):
                self.model.add(self.type_RNN(self.units, activation = self.h_activation, 
                                        return_sequences = True)) #, input_shape = (None, self.asset_num)))
        self.model.add(self.type_RNN(self.units, activation = self.h_activation)) # Dense層に渡す時はreturn_sequences = Falseでなければならない。   
        self.model.add(Dense(self.asset_num,activation = 'linear')) 

        self.model.compile(optimizer = self.optimizer, loss = self.loss)
        es = EarlyStopping(monitor = 'loss',
                           patience = self.patience,
                           verbose = 0)
        if type(X_val) != type(None):
            self.model.fit(X_train,y_train,
                           epochs = 10000,
                           batch_size = 10,
                           verbose = 0,
                           callbacks = [es],
                           validation_data = (X_val,y_val))
        else:
            self.model.fit(X_train,y_train,
                           epochs = 10000,
                           batch_size = 10,
                           verbose = 0,
                           callbacks = [es])
                           
                           
    def predict(self, data): # dataはX_train,X_valと同じ形式をもつ起点データ。ここを起点にして再起的に予測値を出していく
        #X_latest = X_RNN_val[-1].reshape(-1,window_len_RNN,len(df_rate_RNN.columns))
        X_latest = data[-1].reshape(-1,self.window_len,self.asset_num)
        tmp_pred = self.model.predict(X_latest)
        y_pred = tmp_pred.copy()

        for i in range(1,self.forecast_len):
            X_latest = np.append(X_latest, tmp_pred.reshape(1,1,self.asset_num), axis = 1)
            X_latest = np.delete(X_latest,[0,0],axis = 1)
            tmp_pred = self.model.predict(X_latest)
            y_pred = np.append(y_pred,tmp_pred,axis = 0)


        return(y_pred)
        
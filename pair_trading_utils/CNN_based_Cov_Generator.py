import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import EarlyStopping


# CNN based covariance generator
class covGenCNN:
    def __init__(self,
                channels = 1,
                sample_size = 300, # 共分散行列を生成するサンプル数
                stock_num = 10,
                strides = 2,
                padding = 'same',
                use_bias = False,
                pool_size = 3,
                patience = 10,
                filters = 64,
                kernel_size = 7,
                epochs = 10000):
        self.channels = channels
        self.sample_size = sample_size # サンプルにする共分散行列を生成するサンプル数（ややこしいけど、わかるかな？）
        self.stock_num = stock_num
        self.strides = strides
        self.padding = padding
        self.use_bias = use_bias
        self.patience = patience
        self.pool_size = pool_size
        self.filters = filters
        self.kernel_size = kernel_size
        self.epochs = epochs

    def fill_lower_diag(self,a):
        import numpy as np
        n = int((np.sqrt(len(a)*8 + 1) - 1)/2)
        mask = np.tri(n,dtype=bool, k=0) # or np.arange(n)[:,None] >= np.arange(n)
        out = np.zeros((n,n),dtype=float)
        out[mask] = a
        return out  

    # 共分散をLDL分解し，下三角行列と対角成分をベクトル化する関数
    def cov_to_LD_vec(self,cov_mat):
        import numpy as np
        from scipy.linalg import ldl
        # LDL decomposition
        lu,d,perm = ldl(cov_mat)

        # vectorizing matrices
        d_vec = np.log(np.diag(d) + 0.00000001)
        lu_vec = lu[np.tril_indices(len(lu))]
        y_vec = np.append(d_vec,lu_vec)
  
        return(y_vec)

    # LDL分解しベクトル化したベクトルから共分散を復元
    def LD_vec_to_cov(self,LD_vec,stock_num):
        import numpy as np
        from scipy.linalg import ldl
       
        d_vec_re = LD_vec[:stock_num]
        d_re = np.diag(np.exp(d_vec_re))
        lu_vec_re = LD_vec[stock_num:]
        lu_re = self.fill_lower_diag(lu_vec_re)
        cov = lu_re.dot(d_re.dot(lu_re.T))
        return(cov)

    # 'percent'の寄与率をもたらす主成分ベクトルの次元を求める．
    def degree_eig_val(self,eigen_vals,percent):
        sum_ev = sum(eigen_vals)
        exp_power = eigen_vals[0]/sum_ev
        degree = 1
        for i in range(1,len(eigen_vals)):
            exp_power = sum(eigen_vals[:degree])/sum_ev
            if exp_power >= percent:
                break
            else:
                degree += 1
        return(degree) 

    # 共分散行列のサンプルを生成する
    def genSampleCov(self,
                     data):
        import numpy as np
        covs = np.zeros([1,self.stock_num,self.stock_num])
        vecs = np.array([self.cov_to_LD_vec(covs[0])])
        try:
            for i in range(len(data) - self.sample_size + 1):
                covs = np.append(covs,[data.iloc[i:i+self.sample_size,:self.stock_num].cov()],axis = 0) 

            #  vecs = [cov_to_LD_vec(covs[i])] # これはi = 0にすべきでは？
            for i in range(1,len(covs)):
                vecs = np.vstack([vecs,[self.cov_to_LD_vec(covs[i])]])
            covs = np.delete(covs,obj = 0,axis = 0)
            vecs = np.delete(vecs,obj = 0, axis = 0)
        except:
            print('data length might be less than sample size for generating each covariance matrix')
        return([covs,vecs])

    def compile(self,
              optimizer = None,
              loss = None):
        self.loss = loss
        self.optimizer = optimizer


#    def calcCovCNN_(self, X_train, y_train, X_val, y_val):
#        model = Sequential()
#        model.add(tf.keras.layers.Conv2D(64,7,
#                                         strides = self.strides, 
#                                         input_shape = [self.stock_num,self.stock_num,self.channels],
#                                         padding = 'same', 
#                                         use_bias = self.padding))
#        model.add(tf.keras.layers.BatchNormalization())
#        model.add(tf.keras.layers.Activation('relu'))
#        model.add(tf.keras.layers.MaxPool2D(pool_size = 3, strides = self.strides , padding = self.padding))
#        prev_filters = 64
#        for filters in [64] * 3 + [128] * 4 + [256] * 6 + [512] * 3:
#            strides = 1 if filters == prev_filters else 2
#            model.add(ResidualUnit(filters, strides = strides))
#            prev_filters = filters 
#        model.add(tf.keras.layers.GlobalAveragePooling2D())
#        model.add(tf.keras.layers.Flatten())
#        model.add(tf.keras.layers.Dense(self.stock_num + self.stock_num*(self.stock_num + 1)/2,activation='linear'))

##        optimizer = tf.keras.optimizers.SGD(learning_rate = 0.2, momentum = 0.9, decay = 0.01)
#        model.compile(optimizer = self.optimizer,loss = self.loss)
#        es = EarlyStopping(monitor = 'loss',
#                     patience = self.patience,
#                     verbose = 0)
#        tf.random.set_seed(0)
#        model.fit(X_train,y_train,epochs = 5,
#          callbacks = [es],
#          validation_data = (X_val,y_val))
#        cov = model.predict(X_val[len(X_val)-1].reshape(-1,self.stock_num,self.stock_num,self.channels))
#        cov =  LD_vec_to_cov(cov[0],stock_num)
#        return(cov)


    def fit(self, train_data,
            val_data = None):
        
        X_train,y_train = self.genSampleCov(train_data)
        X_val, y_val = self.genSampleCov(val_data)
        model = Sequential()
        model.add(tf.keras.layers.Conv2D(self.filters,
                                         self.kernel_size,
                                         strides = self.strides, 
                                         input_shape = [self.stock_num,self.stock_num,self.channels],
                                         padding = self.padding, 
                                         use_bias = self.use_bias))
        model.add(tf.keras.layers.BatchNormalization())
        model.add(tf.keras.layers.Activation('relu'))
        model.add(tf.keras.layers.MaxPool2D(pool_size = self.pool_size, strides = self.strides , padding = self.padding))
        prev_filters = 64
        for filters in [64] * 3 + [128] * 4 + [256] * 6 + [512] * 3:
            strides = 1 if filters == prev_filters else 2
            model.add(ResidualUnit(filters, strides = strides))
            prev_filters = filters 
        model.add(tf.keras.layers.GlobalAveragePooling2D())
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(self.stock_num + self.stock_num*(self.stock_num + 1)/2,activation='linear'))

#        optimizer = tf.keras.optimizers.SGD(learning_rate = 0.2, momentum = 0.9, decay = 0.01)
        model.compile(optimizer = self.optimizer,loss = self.loss)
        es = EarlyStopping(monitor = 'loss',
                     patience = self.patience,
                     verbose = 0)
        tf.random.set_seed(0)
        model.fit(X_train,y_train,epochs = self.epochs,
          callbacks = [es],
          validation_data = (X_val,y_val))
        
        self.model = model

    def calcCovCNN(self,data):
        X_train,y_train = self.genSampleCov(data)
        cov = self.model.predict(X_train[len(X_train)-1].reshape(-1,self.stock_num,self.stock_num,self.channels))
        cov = self.LD_vec_to_cov(cov[0],self.stock_num)
        return(cov)

    def evaluate(self,data):
        X_val, y_val = self.genSampleCov(data)
        evaluated_value = self.model.evaluate(X_val)

        return(evaluated_value)

# ResNet-34 CNN
class ResidualUnit(tf.keras.layers.Layer):

  def __init__(self,filters,strides=1,activation='relu',**kwargs):
    super().__init__(**kwargs)
    self.filters = filters
    self.strides = strides
    self.activation = tf.keras.activations.get(activation)
    self.main_layers = [
                        tf.keras.layers.Conv2D(filters,3,strides = strides,
                                            padding = 'same', use_bias = False),
                        tf.keras.layers.BatchNormalization(),
                        self.activation,
                        tf.keras.layers.Conv2D(filters,3,strides = strides,
                                            padding = 'same', use_bias = False),
                        tf.keras.layers.BatchNormalization()                    
    ]
    self.skip_layers = []
    if strides > 1:
      self.skip_layers = [
                          tf.keras.layers.Conv2D(filters,1, strides = strides,
                                              padding = 'same', use_bias = False),
                          tf.keras.layers.BatchNormalization()
      ]

    def call(self, inputs):
      Z = inputs
      for layer in self.main_layers:
        Z = layer(Z)
      skip_Z = inputs
      for layer in self.skip_layers:
        skip_Z = layer(skip_Z)
      return self.activation(Z + skip_Z)
  
  def get_config(self):
        config = {
            "filters" : self.filters,
            "strides" : self.strides,
            "activation" : self.activation,
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))

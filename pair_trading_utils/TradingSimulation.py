import numpy as np

def buyandhold(series,mu,sigma):
    port_val = 1
    ser_port_val = [port_val]
    pos = 0 #no-position
    ave_ret = 0
    for ser in series:
        if pos == 0:
            if ser < mu - 2*sigma:
                pos = 1 
                period = 1
        elif pos == 1: # long position
            port_val *= (1 + ser)
            ave_ret = (ave_ret*(period - 1) + ser)/period
            period += 1            
            if ave_ret > mu:
                pos = 0
                period = 0

        ser_port_val = np.append(ser_port_val,port_val)
        

    return(ser_port_val)
    
    
def longshort(series,mu,vol):
  ret = []
  pos = 0 # 0 is no position, 1 is long, -1 is short 
  port_val = 1 # portfolio value 
  for i in range(len(series)):
    if pos == 0:
      if series[i] - mu >= 2*vol: # Expected value is high, short!
        pos = -1
      if series[i] - mu <= -2*vol: # Expected value is low, long!
        pos = 1
    elif pos == 1:
      port_val *= (1 + series[i])
      ret = np.append(ret,series[i])
      if series[i] <= mu:
        pos = 0
    elif pos == -1:
      port_val *= (1 - series[i])
      ret = np.append(ret,-series[i])
      if series[i] >= mu:
        pos = 0
        

  return(port_val,ret)
   